package com.entfrm.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.entfrm.system.entity.Shortcut;
import com.entfrm.system.mapper.ShortcutMapper;
import com.entfrm.system.service.ShortcutService;
import org.springframework.stereotype.Service;

/**
 * @author entfrm
 * @date 2019-08-25 22:56:58
 *
 * @description 快捷方式Service业务层
 */
@Service
public class ShortcutServiceImpl extends ServiceImpl<ShortcutMapper, Shortcut> implements ShortcutService {

}
