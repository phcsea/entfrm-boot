package com.entfrm.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entfrm.system.entity.Datasource;

/**
 * <p>
 * 数据源 Mapper 接口
 * </p>
 *
 * @author entfrm
 * @since 2019-01-30
 */
public interface DatasourceMapper extends BaseMapper<Datasource> {

}
