package com.entfrm.toolkit.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entfrm.toolkit.entity.Apiinfo;

/**
 * @author entfrm
 * @date 2020-04-24 22:18:00
 * @description 接口Service接口
 */
public interface ApiinfoService extends IService<Apiinfo> {
}
