package com.entfrm.security.config;

import cn.dev33.satoken.context.SaHolder;
import cn.dev33.satoken.filter.SaServletFilter;
import cn.dev33.satoken.interceptor.SaInterceptor;
import cn.dev33.satoken.router.SaRouter;
import cn.dev33.satoken.stp.StpUtil;
import cn.dev33.satoken.util.SaResult;
import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.json.JSONUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import java.util.Locale;

/**
 * Sa-Token 代码方式进行配置
 * @author entfrm
 */
@Configuration
public class SaTokenConfig implements WebMvcConfigurer {

	@Value("${entfrm.profile}")
	private String profile;

	@Value("${security.httpIgnores}")
	private String httpIgnores;

	@Value("${security.webIgnores}")
	private String webIgnores;

	/**
	 * 注册 Sa-Token 的拦截器，打开注解式鉴权功能
	 */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
		//注册 Sa-Token 的拦截器，打开注解式鉴权功能
		registry.addInterceptor(new SaInterceptor()).addPathPatterns("/**").excludePathPatterns(CharSequenceUtil.splitToArray(httpIgnores, ","));
		// 注册Locale拦截器(国际化)
		registry.addInterceptor(getLocaleChangeInterceptor());
    }

	/**
	 * Locale拦截器(国际化)
	 *
	 * @return
	 */
	public LocaleChangeInterceptor getLocaleChangeInterceptor() {
		// 定义一个拦截器
		LocaleChangeInterceptor interceptor = new LocaleChangeInterceptor();
		// 拦截请求中 key 为 lang 的参数
		interceptor.setParamName("lang");

		return interceptor;
	}

	/**
	 * 默认解析器 其中locale表示默认语言
	 */
	@Bean
	public SessionLocaleResolver localeResolver() {
		//替换掉默认的 AcceptHeaderLocaleResolver
		SessionLocaleResolver localeResolver = new SessionLocaleResolver();
		//设置默认语言为简体中文
		localeResolver.setDefaultLocale(Locale.SIMPLIFIED_CHINESE);
		return localeResolver;
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		/** 本地文件上传路径 */
		registry.addResourceHandler("/profile/**").addResourceLocations("file:" + profile + "/");
	}

	/**
	 * 开启跨域
	 */
	@Override
	public void addCorsMappings(CorsRegistry registry) {
		// 设置允许跨域的路由
		registry.addMapping("/**")
				// 设置允许跨域请求的域名
				.allowedOriginPatterns("*")
				// 是否允许证书（cookies）
				.allowCredentials(true)
				// 设置允许的方法
				.allowedMethods("*")
				// 跨域允许时间
				.maxAge(3600);
	}

	/**
	 * 注册 Sa-Token全局过滤器，解决跨域问题
	 */
	@Bean
	public SaServletFilter getSaServletFilter() {
		return new SaServletFilter()
				// 拦截与排除 path
				.addInclude("/**").addExclude(CharSequenceUtil.splitToArray(webIgnores, ","))

				// 全局认证函数
				.setAuth(obj -> {
					// 登录校验
					SaRouter.match("/**").notMatch(CharSequenceUtil.splitToArray(httpIgnores, ",")).check(r -> StpUtil.checkLogin());
				})

				// 异常处理函数
				.setError(e -> {
					// 设置响应头
					SaHolder.getResponse().setHeader("Content-Type", "application/json;charset=UTF-8");
					// 使用封装的 JSON 工具类转换数据格式
					return JSONUtil.toJsonStr(SaResult.error(e.getMessage()));
				})

				// 前置函数：在每次认证函数之前执行
				.setBeforeAuth(obj -> {
					// ---------- 设置一些安全响应头 ----------
					SaHolder.getResponse()
							// 服务器名称
							.setServer("entfrm-server")
							// 是否可以在iframe显示视图： DENY=不可以 | SAMEORIGIN=同域下可以 | ALLOW-FROM uri=指定域名下可以
							.setHeader("X-Frame-Options", "SAMEORIGIN")
							// 是否启用浏览器默认XSS防护： 0=禁用 | 1=启用 | 1; mode=block 启用, 并在检查到XSS攻击时，停止渲染页面
							.setHeader("X-XSS-Protection", "1; mode=block")
							// 禁用浏览器内容嗅探
							.setHeader("X-Content-Type-Options", "nosniff");
				});
	}
}
